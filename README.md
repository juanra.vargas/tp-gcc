### Trabajo Practico de la Materia Gestion de Centro de Computos 

- Desplegado en: 
    - Desarrollo: https://tp-gcc-dev.herokuapp.com/
        - Tareas: 
            - Intalacion de paquetes necesarios para el funcionamiento correcto de la aplicacion. 
            - Despliega la version de la aplicacion de la rama `developer` en un servidor de desarollo.    
    - Homologacion: 
        - Tareas: 
            - Ambiente de pruebas unitarias para ambos entornos. 
    - Produccion: https://tp-gcc.herokuapp.com/
        - Tareas: 
            - Despliega la version de la aplicacion de la rama `master` en un servidor de produccion. 

- Aplicacion: 
    - Usuarios y passwords aceptados: 
        - juanVargas: 12345
        - juan.vargas: 123456
        - mathias.vargas: mathu23t
        - pepe.perez: pepe 
    - Paginas
        - Principal: Pagina que muestra un `resume` o `CV`. 
        - Login incorrecto: Despliega un mensaje en caso que el login sea incorrecto. 