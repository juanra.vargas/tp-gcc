const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.get("/", (req, res) => {
    const html = `<html>

    <head>
        <title>Pagina de Login</title>
    </head>
    
    <body>
        <form name="loginForm" method="post" action="/login">
            <table width="20%" bgcolor="0099CC" align="center">
    
                <tr>
                    <td colspan=2>
                        <center>
                            <font size=4><b>Ingrese sus credenciales</b></font>
                        </center>
                    </td>
                </tr>
    
                <tr>
                    <td>Usuario:</td>
                    <td><input type="text" required size=25 name="userid"></td>
                </tr>
    
                <tr>
                    <td>Password:</td>
                    <td><input type="Password" required size=25 name="pwd"></td>
                </tr>
    
                <tr>
                    <td><input type="Reset" value="Borrar"></td>
                    <td><input type="submit" value="Ingresar"></td>
                </tr>
    
            </table>
        </form>
    </body>`;
    res.status(200).send(html);
});


app.post('/login', (req, res) => {
    let usersNpsw = {
        "juanVargas": "12345",
        "juan.vargas": "123456",
        "mathias.vargas": "mathu23t",
        "pepe.perez": "pepe"
    }
    const templateOk = `<div id="header"></div>
    <div class="left"></div>
    <div class="stuff">
        <br><br>
        <h1>Resume</h1>
        <h2>Juan</h2>
        <hr />
        <br>
        <p class="head">Interests</p>
        <ul>
            <li>Drawing</li>
            <li>Photography</li>
            <li>Design</li>
            <li>Programming</li>
            <li>Computer Science</li>
        </ul>
        <p class="head">Skills</p>
        <ul>
            <li>Web Design with HTML & CSS</li>
        </ul>
        <p class="head">Education</p>
        <ul>
            <a href="http://www.wiltonhighschool.org/pages/Wilton_High_School">
                <li>Wilton High School</li>
            </a>
            <!--Link-->
            <a href="https://www.silvermineart.org/">
                <li>Silvermine School of Arts</li>
            </a>
            <li>Codeacademy</li>
        </ul>
        <p class="head">Experience</p>
        <ul>
            <li>Student Technology Intern for Wilton School District</li>
            <li>Babysitter</li>
        </ul>
        <p class="head">Extracurriculars</p>
        <ul>
            <li>Recycling Club</li>
            <li>Gardening Club</li>
            <li>Book Club</li>
        </ul>
    </div>
    <div class="right"></div>
    <div id="footer">
        <h2 id="name">Juan</h2>
    </div>` ;

    if (usersNpsw[req.body.userid] == req.body.pwd) {
        console.log('login exitoso');
        return res.status(302).send(templateOk);
    }
    console.log('login fallido');
    res.status(403).send('<h1>Ops! Credenciales incorrectas</h1>')
});
module.exports = app;