//const express = require("express");
const app = require('./index');
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

const PORT = process.env.PORT || 5000;
app.listen(PORT, function () {
    console.log(`Escuchando en puerto ${PORT}`);
});