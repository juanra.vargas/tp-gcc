const test = require('tape');
const request = require('supertest');
const app = require('../index');

test('first test', function(t) {
    t.end();
});
test('GET /', function(t) {
    request(app)
        .get('/')
        .expect('Content-Type', 'text/html; charset=utf-8')
        .expect(200)
        .end(function(err, res) {
            t.error(err, 'No error');
            t.end();
        });
});
test('POST /login', function(t) {
    request(app)
        .post('/login')
        .set('Content-Type', 'application/json')
        .send({ userid: 'juan.vargas', pwd: '123456' })
        .expect(302)
        .end(function(err, res) {
            t.error(err, 'No error')
            t.end();
        })
});

test('POST login failure', function(t) {
    request(app)
        .post('/login')
        .set('Content-Type', 'application/json')
        .send({ userid: 'juan.vargas', pwd: '743274' })
        .expect(403) // must be 403
        .end(function(err, res) {
            t.error(err, 'No error')
            t.end();
        })
});